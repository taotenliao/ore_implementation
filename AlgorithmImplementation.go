package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"gonum.org/v1/gonum/mat"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"math/rand"
	"os"
	"time"
)

type TestTable struct {
	Id         int
	Ore        string
	Sgx        string
	BinaryData []byte
	Org        float64
}

func (*TestTable) TableName() string {
	return "test_table"
}

// float64SliceToBase64 takes a slice of float64 and returns its base64 representation.
func float64SliceToBase64(floatSlice []float64) string {
	// Create a buffer to write our bytes into
	buf := new(bytes.Buffer)

	// Write the float64 slice into the buffer as binary data
	for _, floatVal := range floatSlice {
		err := binary.Write(buf, binary.LittleEndian, floatVal)
		if err != nil {
			return ""
		}
	}

	// Encode the binary data into a base64 string
	return base64.StdEncoding.EncodeToString(buf.Bytes())
	//return hex.EncodeToString(buf.Bytes()), nil
}

// base64ToFloat64Slice takes a base64 encoded string and returns a slice of float64.
func base64ToFloat64Slice(base64String string) ([]float64, error) {
	// Decode the base64 string
	data, err := base64.StdEncoding.DecodeString(base64String)
	if err != nil {
		return nil, err
	}

	// Create a buffer from the decoded data
	buf := bytes.NewReader(data)

	// The slice to hold the decoded float64 values
	var floatSlice []float64

	// Read the bytes into float64 values
	for {
		var floatVal float64
		err := binary.Read(buf, binary.LittleEndian, &floatVal)
		if err != nil {
			break
		}
		floatSlice = append(floatSlice, floatVal)
	}

	return floatSlice, nil
}

//// InsertDataToDB 数据库连接测试
//func InsertDataToDB(data string) {
//	// 创建数据库连接
//	dsn := "ttl:1234565@tcp(192.168.134.132:3306)/indextest?charset=utf8mb4&parseTime=True&loc=Local"
//	_, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
//	if err != nil {
//		log.Fatal("failed to connect database:", err)
//	}
//}

// seedWithKey 用给定的密钥和种子来初始化伪随机数生成器
func seedWithKey(key string, seed int64) *rand.Rand {
	// 创建一个基于SHA-256的散列实例
	hasher := sha256.New()
	// 将密钥转换为字节并写入散列
	hasher.Write([]byte(key))
	// 将种子转换为字节并写入散列
	seedBytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(seedBytes, uint64(seed))
	hasher.Write(seedBytes)
	// 计算散列值并获取前8个字节作为新的种子
	hash := hasher.Sum(nil)
	newSeed := int64(binary.LittleEndian.Uint64(hash[:8]))
	// 使用新的种子来创建一个伪随机数生成器
	return rand.New(rand.NewSource(newSeed))
}

// 数组洗牌
func shuffle(slice []int, seed int64) {
	rand.Seed(seed)
	// 遍历切片，交换每个元素与一个随机位置的元素
	for i := range slice {
		j := rand.Intn(i + 1)                   // 生成0到i之间的随机数
		slice[i], slice[j] = slice[j], slice[i] // 交换元素
	}
}

// GenerateS 生成L1和L2串
func GenerateS(length int, seed int64) []int {
	var L1 []int
	haf := length / 2
	for i := 0; i < haf; i++ {
		L1 = append(L1, 1)
	}
	for i := 0; i < haf; i++ {
		L1 = append(L1, 0)
	}
	shuffle(L1, seed)
	return L1
}

// GenerateC 生成随机的C串
func GenerateC(length int, r *rand.Rand) []float64 {
	var CArray []float64
	for i := 0; i < length; i++ {
		CArray = append(CArray, r.Float64())
	}
	return CArray
}

// GenerateIndexArray 生成索引向量
func GenerateIndexArray(a float64, L []int, C1 []float64) []float64 {
	var indexArray []float64
	var x = 0
	var y = 0
	for _, value := range L {
		if value == 1 {
			indexArray = append(indexArray, a*float64(C1[x]))
			x++
		} else {
			indexArray = append(indexArray, float64(C1[y]))
			y++
		}
	}
	return indexArray
}

// GenerateQueryVector 生成查询向量
func GenerateQueryVector(b float64, L []int, C1_ []float64) []float64 {
	var queryArray []float64
	var x = 0
	var y = 0
	for _, value := range L {
		if value == 1 {
			queryArray = append(queryArray, float64(-C1_[x]))
			x++
		} else {
			queryArray = append(queryArray, b*float64(C1_[y]))
			y++
		}
	}
	return queryArray
}

// SumOfVector 向量求和
func SumOfVector(indexArray []float64, queryArray []float64) float64 {
	var sum = 0.0
	for i := 0; i < len(indexArray); i++ {
		sum += indexArray[i] * queryArray[i]
	}
	return sum
}

// SplitVectorOfIndexArray 将索引向量分割为A、B向量
func SplitVectorOfIndexArray(indexArray []float64, S []int, seed int64, matrix1 *mat.Dense, matrix2 *mat.Dense) (mat.VecDense, mat.VecDense) {
	var indexArrayA []float64
	var indexArrayB []float64
	rand.Seed(seed)

	for index, value := range indexArray {
		if S[index] == 1 {
			indexArrayA = append(indexArrayA, float64(rand.Intn(4096)))
			indexArrayB = append(indexArrayB, float64(value)-indexArrayA[index])
		} else {
			indexArrayA = append(indexArrayA, float64(value))
			indexArrayB = append(indexArrayB, float64(value))
		}
	}
	var resulA mat.VecDense
	var resulB mat.VecDense
	vecA := mat.NewVecDense(len(indexArrayA), indexArrayA)
	vecB := mat.NewVecDense(len(indexArrayA), indexArrayB)
	resulA.MulVec(matrix1, vecA)
	resulB.MulVec(matrix2, vecB)
	return resulA, resulB
}

// SplitVectorOfQueryArray 将查询向量分割为A、B向量
func SplitVectorOfQueryArray(queryArray []float64, S []int, seed int64, matrix1 *mat.Dense, matrix2 *mat.Dense) (mat.VecDense, mat.VecDense) {
	var queryArrayA []float64
	var queryArrayB []float64
	rand.Seed(seed)
	for index, value := range queryArray {
		if S[index] == 0 {
			queryArrayA = append(queryArrayA, float64(rand.Intn(4096)))
			queryArrayB = append(queryArrayB, float64(value)-queryArrayA[index])
		} else {
			queryArrayA = append(queryArrayA, float64(value))
			queryArrayB = append(queryArrayB, float64(value))
		}
	}
	var resulA mat.VecDense
	var resulB mat.VecDense
	vecA := mat.NewVecDense(len(queryArrayA), queryArrayA)
	vecB := mat.NewVecDense(len(queryArrayB), queryArrayB)
	resulA.MulVec(matrix1, vecA)
	resulB.MulVec(matrix2, vecB)
	return resulA, resulB
}

// GenerateRandomMatrix 生成一个指定大小的可逆矩阵
func GenerateRandomMatrix(size int) *mat.Dense {

	data := make([]float64, size*size)
	for i := range data {
		//data[i] = float64(rand.Intn(33))
		data[i] = rand.Float64()
	}
	m := mat.NewDense(size, size, data)
	var inv mat.Dense
	if inv.Inverse(m) == nil {
		return m
	}
	return GenerateRandomMatrix(size)
}
func SumOfMatrix(pA mat.VecDense, pB mat.VecDense, qA mat.VecDense, qB mat.VecDense) float64 {
	var pAT mat.Dense
	pAT.CloneFrom(pA.T())
	var pBT mat.Dense
	pBT.CloneFrom(pB.T())

	var resulA mat.VecDense
	var resulB mat.VecDense
	resulA.MulVec(&pAT, &qA)
	resulB.MulVec(&pBT, &qB)
	data1 := resulA.RawVector().Data
	data2 := resulB.RawVector().Data
	return data1[0] + data2[0]
}

// GenerateMatrixPair 生成密钥矩阵，并在指定范围内进行测试
// func GenerateMatrixPair(min int, max int, m1SaveFile string, m2SaveFile string, length int, L1 []int, L2 []int, S []int, C1 []float64, C1_ []float64, seed1 int64, seed2 int64) {
func GenerateMatrixPair(m1SaveFile string, m2SaveFile string, length int) {
	rand.Seed(time.Now().UnixNano())
	M1 := GenerateRandomMatrix(length)
	M2 := GenerateRandomMatrix(length)
	fmt.Println(M1)
	fmt.Println(M2)
	var matrix1T, matrix2T, matrix1I, matrix2I mat.Dense
	matrix1T.CloneFrom(M1.T())
	matrix2T.CloneFrom(M2.T())
	matrix1I.Inverse(M1)
	matrix2I.Inverse(M2)
	//counter1 := 0
	//counter2 := 0
	//bar := pb.StartNew(max * max)
	//for i := min; i < max; i++ {
	//	var a = float64(max - i)
	//	for j := min; j < max; j++ {
	//		var b = float64(j)
	//		indexArray := GenerateIndexArray(a, L1, L2, C1)
	//		queryArray := GenerateQueryVector(b, L1, L2, C1_)
	//		org := float64(SumOfVector(indexArray, queryArray))
	//		indexArrayA, indexArrayB := SplitVectorOfIndexArray(indexArray, S, seed1, &matrix1T, &matrix2T)
	//		queryArrayA, queryArrayB := SplitVectorOfQueryArray(queryArray, S, seed2, &matrix1I, &matrix2I)
	//		res := SumOfMatrix(indexArrayA, indexArrayB, queryArrayA, queryArrayB)
	//		if org*res > 0 || org*res == 0 {
	//			counter1++
	//			if math.Abs(org-res) < 1 {
	//				counter2++
	//			}
	//		} else {
	//			fmt.Println("测试失败")
	//			fmt.Println(counter1)
	//			fmt.Println(counter2)
	//			fmt.Println(a)
	//			fmt.Println(b)
	//			fmt.Println(math.Abs(org))
	//			fmt.Println(math.Abs(res))
	//			return
	//		}
	//		bar.Increment()
	//	}
	//}
	//InsertDataToDB()
	//fmt.Println(counter1)
	//fmt.Println(counter2)
	//fmt.Println(float64(counter1) / float64(max*max))
	//fmt.Println(float64(counter2) / float64(max*max))
	// 写入文件
	dataM1 := M1.RawMatrix().Data
	file1, _ := os.Create(m1SaveFile)
	for _, v := range dataM1 {
		binary.Write(file1, binary.LittleEndian, v)
	}
	dataM2 := M2.RawMatrix().Data
	file2, _ := os.Create(m2SaveFile)
	for _, v := range dataM2 {
		binary.Write(file2, binary.LittleEndian, v)
	}
	//bar.Finish()
}

// 加载密钥矩阵
func readFloat64Array(filePath string) ([]float64, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// 获取文件大小
	fileInfo, err := file.Stat()
	if err != nil {
		return nil, err
	}
	fileSize := fileInfo.Size()

	// 读取文件内容到 float64 数组
	readFloat64Array := make([]float64, fileSize/8) // 一个 float64 占用 8 个字节
	err = binary.Read(file, binary.LittleEndian, &readFloat64Array)
	if err != nil {
		return nil, err
	}

	return readFloat64Array, nil
}

// Encrypt 加密数据
func Encrypt(a float64, mKey MKey, subKey string) []float64 {
	L := mKey.L
	S := mKey.S
	var matrix1T, matrix2T, matrix1I, matrix2I mat.Dense
	matrix1T.CloneFrom(mKey.M1.T())
	matrix2T.CloneFrom(mKey.M2.T())
	matrix1I.Inverse(mKey.M1)
	matrix2I.Inverse(mKey.M2)
	seed := time.Now().UnixNano()
	myRand := seedWithKey(subKey, seed)
	C1 := GenerateC(len(L)/2, myRand)
	C1_ := GenerateC(len(L)/2, seedWithKey(subKey, myRand.Int63()))
	indexArray := GenerateIndexArray(a, L, C1)
	queryArray := GenerateQueryVector(a, L, C1_)
	indexArrayA, indexArrayB := SplitVectorOfIndexArray(indexArray, S, rand.Int63(), &matrix1T, &matrix2T)
	queryArrayA, queryArrayB := SplitVectorOfQueryArray(queryArray, S, rand.Int63(), &matrix1I, &matrix2I)
	data := append(indexArrayA.RawVector().Data, indexArrayB.RawVector().Data...)
	data = append(data, queryArrayA.RawVector().Data...)
	data = append(data, queryArrayB.RawVector().Data...)
	return data
}

// Decrypt 解密数据
func Decrypt(data []float64, M1 *mat.Dense, M2 *mat.Dense, S []int, L1 []int, L2 []int, length int) float64 {
	var qurrayA []float64
	var qurrayB []float64
	for i := length * 2; i < length*3; i++ {
		qurrayA = append(qurrayA, data[i])
	}
	for i := length * 3; i < length*4; i++ {
		qurrayB = append(qurrayB, data[i])
	}
	vecAT := mat.NewVecDense(length, qurrayA)
	var resulQA mat.VecDense
	resulQA.MulVec(M1, vecAT)
	vecBT := mat.NewVecDense(length, qurrayB)
	var resulQB mat.VecDense
	resulQB.MulVec(M2, vecBT)
	var qurrayArray []float64
	for i := 0; i < length; i++ {
		if S[i] != 0 {
			qurrayArray = append(qurrayArray, resulQA.RawVector().Data[i])
		} else {
			qurrayArray = append(qurrayArray, resulQB.RawVector().Data[i]+resulQA.RawVector().Data[i])
		}
	}
	var b float64
	for i := 0; i < len(L2); i++ {
		if L2[i] == 0 {
			b = qurrayArray[len(L1)+i]
			break
		}
	}
	return b
}

// Cmp 加密比较密文
func Cmp(a float64, b float64, matrix1T *mat.Dense, matrix2T *mat.Dense, matrix1I *mat.Dense, matrix2I *mat.Dense, L []int, S []int, C1 []float64, C1_ []float64, seed1 int64, seed2 int64) (float64, float64) {
	indexArray := GenerateIndexArray(a, L, C1)
	queryArray := GenerateQueryVector(b, L, C1_)
	org := float64(SumOfVector(indexArray, queryArray))
	indexArrayA, indexArrayB := SplitVectorOfIndexArray(indexArray, S, seed1, matrix1T, matrix2T)
	queryArrayA, queryArrayB := SplitVectorOfQueryArray(queryArray, S, seed2, matrix1I, matrix2I)
	res := SumOfMatrix(indexArrayA, indexArrayB, queryArrayA, queryArrayB)
	//fmt.Println(org)
	//fmt.Println(res)
	return org, res
}

type MKey struct {
	M1 *mat.Dense
	M2 *mat.Dense
	L  []int
	S  []int
}

// GenerateMKey 生成密钥
func GenerateMKey(M1file string, M2file string, length int, seedL int64, seedS int64) (key MKey) {
	M1Data, _ := readFloat64Array(M1file)
	M2Data, _ := readFloat64Array(M2file)
	M1 := mat.NewDense(length, length, M1Data)
	M2 := mat.NewDense(length, length, M2Data)
	L := GenerateS(length, seedL)
	S := GenerateS(length, seedS)
	return MKey{M1, M2, L, S}
}
func InitDB() *gorm.DB {
	// 初始化数据库
	dsn := "ttl:123456@tcp(192.168.80.131:3306)/indextest?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("failed to connect database:", err)
	}
	return db
}

//func InsertPlainNumber(plaintext float64, length int, db *gorm.DB, mKey MKey, subKey string) {
//	//M1, M2, L1, L2, C1, C2, S := GenerateMKey("./Matrix1_test.bin", "./Matrix2_test.bin", length, 888, time.Now().UnixNano(), rand.Int63n(time.Now().UnixNano()), 1232)
//	//GenerateMatrixPair(0, 100000, "./Matrix1_test.bin", "./Matrix2_test.bin", length, L1, L2, S, C1, C2, 111, 555)
//	//fmt.Println("明文: ", plaintext)
//	ciphertextArray := Encrypt(plaintext, mKey, subKey)
//	//fmt.Println("密文: ")
//	//fmt.Println(ciphertextArray)
//	baseString, err := float64SliceToBase64(ciphertextArray)
//	if err != nil {
//		fmt.Println(err)
//	}
//	//fmt.Println(baseString)
//	//db.Debug().Create(&Table1{IndexCipe: baseString /*IndexGbk: baseString,*/, IndexNum: plaintext})
//	//db.Debug().Create(&Table2{IndexCipe: baseString /*IndexGbk: baseString,*/, IndexNum: plaintext})
//}

func TempTest1() {
	//insert data
	//db := InitDB()
	////batch := 10000
	////bar := pb.StartNew(batch)
	//mKey := GenerateMKey("./Matrix1_test.bin", "./Matrix2_test.bin", 8, 888, 666)
	////for i := 0; i < batch; i++ {
	////	rand.Seed(time.Now().UnixNano())
	////	plain := rand.Int63n(100000)
	////	//fmt.Println(plain)
	////	InsertPlainNumber(float64(plain), 8, db, key)
	////	bar.Increment()
	////}
	//subKey := "testKey"
	//InsertPlainNumber(100.0, 8, db, mKey, subKey)
	//test data
	//plaintext := 30.0
	//length := 8
	////M1, M2, L1, L2, C1, C2, S := GenerateMKey("./Matrix1.bin", "./Matrix2.bin", length, 888, 666, 999, 1232)
	//M1, M2, L1, L2, C1, C2, S := GenerateMKey("./Matrix1_test.bin", "./Matrix2_test.bin", length, 888, time.Now().UnixNano(), rand.Int63n(time.Now().UnixNano()), 1232)
	////GenerateMatrixPair( "./Matrix1_test.bin", "./Matrix2_test.bin", length, L1, L2, S, C1, C2, 111, 555)
	//var matrix1T, matrix2T, matrix1I, matrix2I mat.Dense
	//matrix1T.CloneFrom(M1.T())
	//matrix2T.CloneFrom(M2.T())
	//matrix1I.Inverse(M1)
	//matrix2I.Inverse(M2)
	//fmt.Println("明文: ", plaintext)
	//ciphertextArray := Encrypt(plaintext, &matrix1T, &matrix2T, &matrix1I, &matrix2I, L1, L2, S, C1, C2, 111, 222)
	//
	//fmt.Println("密文: ")
	//fmt.Println(ciphertextArray)
	//base, err := float64SliceToBase64(ciphertextArray)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println("密文base64:\n ", base)
	//fmt.Println("解密后:", Decrypt(ciphertextArray, M1, M2, S, L1, L2, length))
	//Cmp(49999, 50000, &matrix1T, &matrix2T, &matrix1I, &matrix2I, L1, L2, S, C1, C2, 111, 222)
	//bar := pb.StartNew(1000000)
	//for i := 0; i < 1000000; i++ {
	//	_, res := Cmp(float64(i), float64(i+1), &matrix1T, &matrix2T, &matrix1I, &matrix2I, L1, L2, S, C1, C2, 111, 222)
	//	fmt.Print("\r", int(res))
	//	if math.Abs(res) < 1 {
	//		fmt.Println("测试失败")
	//		fmt.Println("res:", int(res))
	//	}
	//	bar.Increment()
	//}
	//fmt.Println("测试成功")
}

func InsertTestSGXORE(db *gorm.DB, number float64) {
	key := []byte{0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf, 0x10}
	buf := new(bytes.Buffer)
	// 使用binary.Write将float64类型的num转换成[]byte，这里使用小端序
	err := binary.Write(buf, binary.LittleEndian, number)
	if err != nil {
		fmt.Println("binary.Write failed:", err)
	}
	cipherSGX, _ := Aes_Encrypt(buf.Bytes(), key)
	mKey := GenerateMKey("./Matrix1_test.bin", "./Matrix2_test.bin", 8, 888, 666)
	subKey := "testKey"
	cipherFloat := Encrypt(number, mKey, subKey)
	db.Debug().Create(&TestTable{Sgx: base64.StdEncoding.EncodeToString(cipherSGX), BinaryData: cipherSGX, Ore: float64SliceToBase64(cipherFloat), Org: number})
}
func TestMaxInsert(db *gorm.DB, maxItems int, maxRate float64) {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < maxItems; i++ {
		InsertTestSGXORE(db, float64(i)+rand.Float64()*maxRate)
	}
}

func main() {
	db := InitDB()
	InsertTestSGXORE(db, 1543.32545)
}
